<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/posts/{post}', function ($post)  {//wildcard oluşturduk. {} yerine yazılan her değerde yönlendirmesi için.

// });


//Controller oluşturmak için cmd- php artisan make:controller (-r default 7 metodu yüklüyor.)


Route::get('/', function () {
    return view('welcome');
});

Route::get('about', function () {
    // $article = App\Article::all(); //all yerine take(2)->get() yazıp 2 tane article getirebiliriz.
                                    // veya paginate(2) yazarız numaralandırarak getirir.
    $articles = App\Article::latest()->get(); // defaultu order by created_at desc
                                            //latest içine 'published_at' yazarsak ona göre sıralar.
    
    return view('about', ['articles' => $articles
    ]);
});

Route::get('/articles', 'ArticlesController@index')->name('articles.index');
Route::post('/articles', 'ArticlesController@store');
Route::get('/articles/create', 'ArticlesController@create');
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit', 'ArticlesController@edit');
Route::put('/articles/{article}', 'ArticlesController@update');






//Route::get('/articles/{article}/edit', 'ArticlesController@edit');




// Route::get('/contact', function () {
//     return view('contact');
// });


//Route::get('/posts/{post}', 'PostsController@show');
