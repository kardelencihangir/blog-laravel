@extends('layout')

@section('head')
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
@endsection

@section('content')

<div id="wrapper">
	<div id="page" class="container">
        <h1>New Article</h1>
        <form method="POST" action="/articles">
            @csrf {{--  stands for cross-site request forgery --}}
            <div class="form-group">
                <label class="label" for="title">Title</label>

                <div class="control">
                    <input 
                        class="form-control @error('title') @enderror"  
                        type="text" 
                        name="title" 
                        id="title"
                        value="{{old('title')}}"> {{--validation hatasında
                                                    bir önceki girilen değeri tekrar geri
                                                    getirmesi için old fonksiyonunu
                                                    laravel sağlıyor. --}}
                    @error('title')
                    <p class="help">{{$errors->first('title')}}</p>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label class="label" for="excerpt">Excerpt</label>


                <div class="control">
                    <input 
                    class="form-control @error('excerpt') @enderror"  
                    type="text" 
                    name="excerpt" 
                    id="excerpt"
                    value="{{old('excerpt')}}">
                    @error('excerpt')
                    <p class="help">{{$errors->first('excerpt')}}</p>
                    @enderror
                </div>
            </div>

            
            <div class="form-group">
                <label class="label" for="body">Body</label>


                <div class="control">
                    <input
                        class="form-control @error('body') @enderror"  
                        type="text" 
                        name="body" 
                        id="body"
                        value="{{old('body')}}">
                    @error('body')
                    <p class="help">{{$errors->first('body')}}</p>
                    @enderror
                </div>
            </div>


            <div class="form-group">
                <label class="label" for="body">Tags</label>


                <div class="control">
                    <select 
                    name="tags[]" 
                    multiple
                    id="">
                    @foreach ($tags as $tag)
                <option value="{{$tag->id}}">{{$tag->name}}</option>
                        
                    @endforeach

                    </select>
                    @error('tags')
                    <p class="help">{{$message}}</p>
                    @enderror
                </div>
            </div>


            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link" type="submit">Submit</button>
                </div>
            </div>
        </form>

        
    </div>
</div>
@endsection


{{-- formun eski hali  
    
    <form action="">
            <div class="field">
                <label class="label" for="title">Title</label>


                <div class="control">
                    <input class="input" type="text" name="title" id="title">
                </div>
            </div>


            <div class="field">
                <label class="label" for="excerpt">Excerpt</label>


                <div class="control">
                    <textarea name="textarea" name="excerpt" id="excerpt"></textarea>
                </div>
            </div>

            
            <div class="field">
                <label class="label" for="body">Body</label>


                <div class="control">
                    <textarea name="textarea" name="body" id="body"></textarea>
                </div>
            </div>


            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link" type="submit">Submit</button>
                </div>
            </div>
        </form> --}}