@extends('layout')

@section('head')
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
@endsection

@section('content')

<div id="wrapper">
	<div id="page" class="container">
        <h1>Update Article</h1>
    <form method="POST" action="/articles/{{$article->id}}"> {{-- Burayı create'ten farklı değiştirdik
                                                 fakat browserlar put metodunu tam tanıyamadığı
                                                 için bir alttaki @method satırını eklemek
                                                 zorunda kaldık. 
                                                 eğer post ve get dışında metot kullanacaksak
                                                 hep bu şekilde aşağıda yazacağız çünkü 
                                                 diğer türlü çalışmaz--}}
            @csrf {{--  stands for cross-site request forgery --}}
            @method('PUT')
            <div class="form-group">
                <label class="label" for="title">Title</label>

                <div class="control">
                <input class="form-control" type="text" name="title" id="title" value="{{$article->title}}">
                </div>
            </div>


            <div class="form-group">
                <label class="label" for="excerpt">Excerpt</label>


                <div class="control">
                    <input class="form-control" type="text" name="excerpt" id="excerpt" value="{{$article->excerpt}}">
                </div>
            </div>

            
            <div class="form-group">
                <label class="label" for="body">Body</label>


                <div class="control">
                    <input class="form-control" type="text" name="body" id="body" value="{{$article->body}}">
                </div>
            </div>


            <div class="field is-grouped">
                <div class="control">
                    <button class="button is-link" type="submit">Submit</button>
                </div>
            </div>
        </form>

        
    </div>
</div>
@endsection