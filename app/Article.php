<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'excerpt', 'body']; // controller'da direkt tablodakini değiştirmeye
                                                        //çalıştığımız için buraya bunu ekledik.

                                                        //User::create(request()->all()) şeklinde
                                                        //hepsi için kullanmak tehlikeli olabilir.
    public function path() {
        return route('articles.show', $this);
    }                                        
    
    public function user() {

        return $this->belongsTo(User::class, 'user_id');

        // return $this->belongsTo(User::class); //laravel burada direkt diyor ki
        //                                         // foreign key muhtemelen user_id adında.
        //                                         // eğer burada fonksiyon adını author olarak
        //                                         // değiştirirsek author_id adında arayacak
        //                                         // ve bulamadığı için tinker'da arattığımızda
        //                                         // null sonucunu verecek.
        //                                         // bu yüzden ikinci argüman verdik.
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}

// public function getRouteKeyName()
//     {
//         return 'slug'; //Article::where('slug', $article)->first();
//     }