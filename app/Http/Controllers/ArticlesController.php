<?php

namespace App\Http\Controllers;

use App\Article; // bunu sonradan ekledik!!!!
use App\Tag;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index() { // when we list a collection of articles we use index

        if(request('tag')){
            $articles = Tag::where('name', request('tag'))->firstOrFail()->articles;
        }
        else{
            $articles = Article::latest()->get();
        }
        

        return view('articles.index', ['articles' => $articles]);
    }

    public function show(Article $article) {  //buradaki değişken ismi route'daki değişken ismiyle
                                              //aynı olmalı. çünkü laravel öyle arıyor.
        return view('articles.show', ['article' => $article]);
    }

    public function create() {
        return view('articles.create', [
            'tags' => Tag::all()
        ]);
    }

    public function store(){

        $article = new Article($this->validateArticle());
        $article->user_id=1; // auth()->id()
        $article->save();
        $articles->tags()->attach(request('tags'));

        return redirect(route('articles.index'));
        
    }
    public function edit(Article $article) {

        
        return view('articles.edit', compact('article')); //[ 'article' => $article] da yazabilirdik compact fonk. yerine
    }
    public function update(Article $article) {

        $article->update($this->validateArticle());

        return redirect($article->path());

    }
    protected function validateArticle(){
        return request()->validate([
            'title' => 'required',
            'excerpt' => 'required',
            'body' => 'required'
        ]);
    }
}

//store fonksiyonunun eski hali: (bu kodu sonrasında kolaylaştırdık.)
 // $article->title = request('title');
        // $article->excerpt = request('excerpt');
        // $article->body = request('body');
        // $article->save();
        // return view('articles.edit');

        //compact -> php nin sağladığı bir fonksiyon


        // public function store(){

        //     request()->validate([
        //         'title' => 'required',
        //         'excerpt' => 'required',
        //         'body' => 'required'
        //     ]);
    
        //     $article = new Article();
    
        //     $article->title = request('title');
        //     $article->excerpt = request('excerpt');
        //     $article->body = request('body');
        //     $article->save();
    
        //     return redirect('/articles');
            
        // }

        // public function store(){ //sonrasında bu şekilde yaptık fakat alt alta aynı şeyler olduğu için
        //                         //daha sonrasında değiştirdik.
        //     request()->validate([
        //         'title' => 'required',
        //         'excerpt' => 'required',
        //         'body' => 'required'
        //     ]);
    
        //     Article::create([
        //         'title'=> request('title'),
        //         'excerpt'=> request('excerpt'),
        //         'body'=> request('body')
        //     ]);


// ILK SHOW FONKSIYONU
//         public function show($id) { // when we show an article we use show
        
//             // parantez içine Article $id yazarsam direkt veritabanı
//             // içerisinde id'sini obje olarak döndürüyor ve alttaki satıra
//             //gerek kalmıyor.
// $article = Article::findOrFail($id); // bu model ismini direkt olarak küçük harfe dönüştürüp
//         // sonuna çoğul eki -s ekliyor ve o tablodan bakıyor.
//         // önceden bu fonksiyon find idi ama kullanıcı tarayıcıya
//         //var olmayan bir id girerse hata veriyordu.
//         // 404 ekranı vermesi için findOfFailyaptık.

// return view('articles.show', ['article' => $article]);
// }

// public function edit($id) {

//     // find the article associated with the id
//     $article = Article::findOrFail($id); // bunu 3 ayrı fonksiyonda kullanıyor. Bunu kısaltmak
//                                              için sonrasında $id 'yi silip yerine Article $article
//     return view('articles.edit', compact('article')); //[ 'article' => $article] da yazabilirdik compact fonk. yerine
// }

// update için de yukarıdakinin aynısını yaptık.