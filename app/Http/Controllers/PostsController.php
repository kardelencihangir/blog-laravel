<?php

namespace App\Http\Controllers; //namespace altinda use DB; yazarsak DB icin yazarken \ yazmamiza gerek kalmaz
use App\Post;


use Illuminate\Http\Request;

class PostsController extends Controller
{
    
public function show($slug)
{

   // $post = \DB::table('posts')->where('slug', $slug)->first();
//    if (! $post) { // bunu direkt olarak yapıyor. first() kullansak bunu da kullanırdık.
//     abort(404);
// }
    $post = Post::where('slug', $slug)->firstorFail();
    

    return view('post',[
    'post' => $post
    ]);






    // dd($post); Bunu ekrana yazdirip kontrol etmek icin kullaniyoruz.
    // sonrasinda artik bunu view'a yollamaliyiz.
//         $posts = [
//     'my-first-post' => 'Hello, this is my first blog post!',    //m-f-p burada 
//     'my-second-post' => 'Now I am getting the hang of this blogging thing.'    
// ];

// // return view('post', [
// //     'post' => $posts[$post] ?? 'Nothing here yet...' /* Burada ekrana birden fazla postumuz olursa onları
// //     ekrana yazdırmayı kodladık. burada urlye my-first-post veya my-second-post dışında bir şey girildiğinde bu 
// //     hatadan kaçmak için ??'den sonraki kısmı ekledik. Fakat yine de istenmeyen bir değer girildiğinde 404 page
// //     vermeliyiz. Bunun için aşağıdaki kodlamayı yazdık. */

// if(!array_key_exists($post, $posts)) { //key, array
//     abort(404, 'Sorry, that page is not found.');
// }

// return view('post', [
//     'post' => $posts[$post]
                                                        
// ]);


    
}
}
